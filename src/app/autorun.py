##
import requests
import json
import os
from db.migrate import migrate_db
from pbglobal.pblib.autorun_basic import AutorunBasic
from db.comments import Comments

class Autorun(AutorunBasic):

    def __init__(self):
        super(Autorun, self).__init__()

    def post_deployment(self):
        Comments(create_table_if_not_exists=True)
        migrate_db()
        print("Executing initialisation-script")
        print(self.kong_api)
        if self.kong_api == "":
            return

        request = requests.get(self.kong_api)

        if request.status_code == 200:
            self.add_service("commentservice", "http://commentservice.planblick.svc:8000")
            self.add_route(service_name="commentservice", path="/get_comments_by_target", strip_path=False, delete_before_create=True)
        else:
            print("Kong-API not available:", self.kong_api)


import json
from time import sleep
from pbglobal.pblib.amqp import client
import traceback
from config import Config

from pbglobal.events.newCommentCreated import newCommentCreated
from pbglobal.events.commentDeleted import commentDeleted

from db.comments import Comments

class message_handler():
    kong_api = "http://kong.planblick.svc:8001"
    Comment_client = client()

    def handle_message(ch, method=None, properties=None, body=None):
        try:
            print("---------- Start handling Message ----------")
            event = method.routing_key
            body = body.decode("utf-8")
            instance = message_handler()
            if (event is not None):
                handler_exists = method.routing_key in dir(instance)

                if handler_exists is True:
                        if getattr(instance, method.routing_key)(ch, body) == True:
                            ch.basic_ack(method.delivery_tag)
                        else:
                            ch.basic_nack(method.delivery_tag)
                else:
                    raise Exception("No handler for this  message or even not responsible: '" + event + "'")

        except Exception as e:
            print(e)
            traceback.print_exc()
            ch.basic_nack(method.delivery_tag)
            sleep(15)

        print("---------- End handling Message ----------")

    def createNewComment(self, ch, msg):
        print("Handling createNewComment")
        command_payload = json.loads(msg)
        event = newCommentCreated.from_json(newCommentCreated, command_payload)
        event.publish()
        return True

    def newCommentCreated(self, ch, msg):
        try:
            print("Handling newCommentCreated")
            json_data = json.loads(msg)
            new_comment = newCommentCreated.from_json(newCommentCreated, json_data)

            Config.db_session.query(Comments) \
                .filter(Comments.consumer_id == new_comment.owner) \
                .filter(Comments.comment_id == new_comment.comment_id) \
                .delete()

            comment = Comments()
            comment.creator = new_comment.creator
            comment.consumer_id = new_comment.owner
            comment.target_id = new_comment.data.get("target_id")
            comment.comment_id = new_comment.comment_id
            comment.data = json.dumps(new_comment.data)
            comment.data_owners = json.dumps(new_comment.data_owners) if new_comment.data_owners is not None else json.dumps(
                [f"user.{comment.creator}"])
            comment.save()

            message = {"command": "newComment",
                       "args": {"comment_id": new_comment.comment_id, "task_id": json_data.get("correlation_id"), "data": new_comment.data}
                       }

            payload = {"room": comment.consumer_id, "data": message}
            client().publish(toExchange="socketserver", routingKey="commands", message=json.dumps(payload))
        except Exception as e:
            print(e)
            traceback.print_exc()
            Config.db_session.rollback()
            sleep(3)
            return False

        return True

    def deleteComment(self, ch, msg):
        print("Handling deleteComment")
        command_payload = json.loads(msg)
        event = commentDeleted.from_json(commentDeleted, command_payload)
        event.publish()
        return True

    def commentDeleted(self, ch, msg):
        try:
            print("Handling CommentDeleted")
            json_data = json.loads(msg)
            Comment_deleted_event = commentDeleted.from_json(commentDeleted, json_data)
            Config.db_session.query(Comments) \
                .filter(Comments.consumer_id == Comment_deleted_event.owner) \
                .filter(Comments.comment_id == Comment_deleted_event.comment_id) \
                .delete()

            message = {"command": "deletedComment",
                       "args": {"comment_id": Comment_deleted_event.comment_id, "task_id": json_data.get("correlation_id")}}

            payload = {"room": Comment_deleted_event.owner, "data": message}
            client().publish(toExchange="socketserver", routingKey="commands", message=json.dumps(payload))

        except Exception as e:
            print(e)
            traceback.print_exc()
            Config.db_session.rollback()
            sleep(3)
            return False

        return True






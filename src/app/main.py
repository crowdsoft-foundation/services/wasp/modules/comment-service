from flask import Flask, jsonify, request
import json

from pbglobal.pblib.api_calls import ApiCalls
from config import Config
from db.comments import Comments

app = Flask(__name__)

@app.route('/get_comments_by_target/<target_id>', methods=["GET"])
def get_comments_by_target(target_id = None):
    app.config['JSON_SORT_KEYS'] = False
    consumer_id = request.headers.get("X-Consumer-Custom-Id", "UNKNOWN")
    apikey = request.headers.get("apikey", "UNKNOWN")
    results = Config.db_session.query(Comments) \
        .filter(Comments.consumer_id == consumer_id) \
        .filter(Comments.target_id == target_id) \
        .all()

    login = ApiCalls().getLoginByApiKey(apikey)
    requesters_group_memberships = ApiCalls().getGroupMembershipsByApikey(apikey)

    return_value = {}

    for result in results:
        data_owners = json.loads(result.data_owners)
        matches = [match for match in requesters_group_memberships if match in data_owners]
        if f"user.{login}" in data_owners:
            matches.append(f"user.{login}")

        if len(matches) == 0 and len(data_owners) > 0:
            continue

        data = json.loads(result.data)

        return_value[result.comment_id] = {"data": data, "creator": result.creator, "creation_time": result.create_time.strftime('%Y-%m-%d %H:%M:%S')}

    return jsonify({"comments": return_value})


@app.route('/healthz', methods=["GET"])
def health_action():
    return jsonify({"message": "OK"}), 200


@app.before_request
def before_request():
    payload = request.get_json(silent=True)
    if payload is not None:
        payload["correlation_id"] = request.headers.get("correlation_id", "")
        request.data = json.dumps(payload)
